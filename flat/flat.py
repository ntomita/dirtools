import os, shutil
import sys, getopt
import time

"""FLAT
Remove nested folders in a specific folder by moving files to right under the folder.
Ex:
	UpperCase: folders
	LowerCase: files
	(Before)
	A-B-C-a
	   -D-b
	   -E-F-c
	(After applying on A)
	A-a
	 -b
	 -F-c

	Need fix:
	before do something,
	if #folders > 1:
		if each folder has files at root:
			stop
"""
def main():
	cmdargs = sys.argv
	folder_path = cmdargs[1]
	dir_queue = []
	while True:
		reached_to_file = False
		entries = os.listdir(folder_path)
		if len(entries) == 0:
			break
		if len(entries) >= 2:
			num_folders_have_files = 0
			for entry in entries:
				if not os.path.isfile(os.path.join(folder_path, entry)):
					for sub_entry in os.listdir(os.path.join(folder_path, entry)):
						if os.path.isfile(os.path.join(folder_path, entry, sub_entry)):
							num_folders_have_files += 1
							break
			if num_folders_have_files >= 2:
				break

		# CHECK IF FILE IS AT A ROOT OF FOLDERS
		for entry in entries:
			if (not reached_to_file) and os.path.isfile(os.path.join(folder_path, entry)):
				reached_to_file = True
		# IF NOT FILES YET, FOLDER UP EACH FOLDERS
		if not reached_to_file:
			for folder in entries:
				folder_up(os.path.join(folder_path, folder))
		else:
			break



def folder_up(folder_path):
	#reach_to_file = False
	source = os.listdir(folder_path)
	dest_folder_path = os.path.dirname(folder_path)
	temp_dest_folder_path = folder_path + str(int(time.time()))
	while os.path.exists(temp_dest_folder_path):
		temp_dest_folder_path = folder_path + str(int(time.time()))
	os.rename(folder_path, temp_dest_folder_path)
	for file in os.listdir(temp_dest_folder_path):
		file_path = os.path.join(temp_dest_folder_path,file)
		#if !reach_to_file and os.isfile(file_path):
		#	reach_to_file = True
		shutil.move(file_path, dest_folder_path)
	os.rmdir(temp_dest_folder_path)
	#return reach_to_file


main()