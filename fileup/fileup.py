import os, shutil
import sys, getopt
import time

"""FILEUP
Move every file and folder in a specific folder outside of the folder.
Ex:
	UpperCase: folders
	LowerCase: files
	(Before)
	A-B-C-a
	   -D-b
	   -E-F-c
	(After applying on A)
	B-C-a
	 -D-b
	 -E-F-c
	(Then applying on B)
	C-a
	D-b
	E-F-c
"""
def main():
	cmdargs = sys.argv
	folder_path = cmdargs[1]
	
	source = os.listdir(folder_path)
	dest_folder_path = os.path.dirname(folder_path)
	temp_dest_folder_path = folder_path + str(int(time.time()))
	while os.path.exists(temp_dest_folder_path):
		temp_dest_folder_path = folder_path + str(int(time.time()))
	os.rename(folder_path, temp_dest_folder_path)
	for file in os.listdir(temp_dest_folder_path):
		shutil.move(os.path.join(temp_dest_folder_path,file), dest_folder_path)
	os.rmdir(temp_dest_folder_path)

main()