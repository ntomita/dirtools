# EXTENDED CONTEXT MENUS FOR WINDOWS #

With this tool, you can

* move every file and subfolders under a folder outside

* flatten deep nested folders by removing intermediate folders.

from context menu.

Very useful if you need to perform lots of file operations with gui.

## PREREQUISITE: ##

* PYTHON3.5
* CX_FREEZE

### TO BUILD EXECUTABLES, USE FOLLOWING COMMANDS IN TERMINAL ###
In fileup/

```
#!python

$ python setup_fileup.py build
```

or
In flat/

```
#!python

$ python setup_flat.py build
```


### IN ORDER TO INSTALL THE COMMANDS, RUN BAT FILES AS ADMINISTRATOR ### (from right click menu) 

https://www.microsoft.com/en-us/download/details.aspx?id=48145